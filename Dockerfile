FROM docker:20.10.6

RUN apk update
RUN apk -Uuv add make gcc groff less libffi-dev openssl-dev python3-dev py3-pip cargo git
RUN pip install --upgrade pip
RUN pip install awscli
RUN apk add --no-cache --virtual .pynacl_deps build-base
RUN pip install PyNaCl
RUN apk add cairo cairo-dev freetype-dev gdk-pixbuf-dev lcms2-dev musl-dev pango-dev poppler-utils py-cffi rust tcl-dev tk-dev zlib-dev
RUN pip install cryptography
RUN pip install --upgrade --ignore-installed six awsebcli
